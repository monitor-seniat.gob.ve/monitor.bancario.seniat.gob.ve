package com.monitor.proyecto.transaccion.entity;

import java.sql.Date;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "transaccion", schema = "sistebank")
public class Transaccion {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_transaccion;

	@Column(name = "banco", length = 10, nullable = false)
	private String banco;

	@Column(name = "rif", length = 10, nullable = false)
	private String rif;

	@Column(name = "periodo", nullable = false)
	private Date periodo;

	@Column(name = "monto", length = 10, nullable = false)
	private String monto;

	@Column(name = "codigo_procesada", length = 10, nullable = false)
	private String codigo_procesada;

	@Column(name = "nro_documento", length = 10, nullable = false)
	private String nro_documento;

	@Column(name = "forma_documento", length = 10, nullable = false)
	private String forma_documento;

	@Column(name = "id_usuario", length = 10, nullable = false)
	private String id_usuario;

	@Column(name = "agencia", length = 10, nullable = false)
	private String agencia;

	@Column(name = "tipo_transaccion", length = 10, nullable = false)
	private String tipo_transaccion;

	@Column(name = "fecha_transaccion", nullable = false)
	private Date fecha_transaccion;

	public Transaccion() {

	}

	public Transaccion(Long id_transaccion, String banco, String rif, Date periodo, String monto,
			String codigo_procesada, String nro_documento, String forma_documento, String id_usuario, String agencia,
			String tipo_transaccion, Date fecha_transaccion) {
		super();
		this.id_transaccion = id_transaccion;
		this.banco = banco;
		this.rif = rif;
		this.periodo = periodo;
		this.monto = monto;
		this.codigo_procesada = codigo_procesada;
		this.nro_documento = nro_documento;
		this.forma_documento = forma_documento;
		this.id_usuario = id_usuario;
		this.agencia = agencia;
		this.tipo_transaccion = tipo_transaccion;
		this.fecha_transaccion = fecha_transaccion;
	}

	public Long getId_transaccion() {
		return id_transaccion;
	}

	public void setId_transaccion(Long id_transaccion) {
		this.id_transaccion = id_transaccion;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getRif() {
		return rif;
	}

	public void setRif(String rif) {
		this.rif = rif;
	}

	public Date getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Date periodo) {
		this.periodo = periodo;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getCodigo_procesada() {
		return codigo_procesada;
	}

	public void setCodigo_procesada(String codigo_procesada) {
		this.codigo_procesada = codigo_procesada;
	}

	public String getNro_documento() {
		return nro_documento;
	}

	public void setNro_documento(String nro_documento) {
		this.nro_documento = nro_documento;
	}

	public String getForma_documento() {
		return forma_documento;
	}

	public void setForma_documento(String forma_documento) {
		this.forma_documento = forma_documento;
	}

	public String getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(String id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getTipo_transaccion() {
		return tipo_transaccion;
	}

	public void setTipo_transaccion(String tipo_transaccion) {
		this.tipo_transaccion = tipo_transaccion;
	}

	public Date getFecha_transaccion() {
		return fecha_transaccion;
	}

	public void setFecha_transaccion(Date fecha_transaccion) {
		this.fecha_transaccion = fecha_transaccion;
	}

}