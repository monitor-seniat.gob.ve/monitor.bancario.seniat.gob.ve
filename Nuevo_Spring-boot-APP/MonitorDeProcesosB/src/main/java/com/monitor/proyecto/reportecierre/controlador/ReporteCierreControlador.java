package com.monitor.proyecto.reportecierre.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.monitor.proyecto.reportecierre.entity.ReporteCierre;
import com.monitor.proyecto.reportecierre.repository.ReporteCierreRepository;


@RestController
@RequestMapping("/api/v1/")
@CrossOrigin(origins = "http://localhost:4200")
public class ReporteCierreControlador {
	@Autowired
    public ReporteCierreRepository repositorio;

    // Método para listar todos los reporte cierres
    @GetMapping("/reportecierre/")
    public List<ReporteCierre> obtenerListaDeReportecierres() {
        return repositorio.findAll();
    }

}
