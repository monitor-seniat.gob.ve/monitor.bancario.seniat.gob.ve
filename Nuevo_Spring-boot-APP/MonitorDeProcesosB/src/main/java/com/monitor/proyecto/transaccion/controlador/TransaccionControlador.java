package com.monitor.proyecto.transaccion.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.monitor.proyecto.transaccion.entity.Transaccion;
import com.monitor.proyecto.transaccion.repository.TransaccionRepository;

@RestController
@RequestMapping("/api/v1/")
@CrossOrigin(origins = "http://localhost:4200")
public class TransaccionControlador {

    @Autowired
    public TransaccionRepository repositorio;

    // Método para listar todas las transacciones
    @GetMapping("/transaccion/")
    public List<Transaccion> obtenerListaDeTransaccions() {
        return repositorio.findAll();
    }
}