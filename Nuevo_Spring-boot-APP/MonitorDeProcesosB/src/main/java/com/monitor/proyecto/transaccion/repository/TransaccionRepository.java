package com.monitor.proyecto.transaccion.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.monitor.proyecto.transaccion.entity.Transaccion;

public interface TransaccionRepository extends JpaRepository<Transaccion, Long> {

}
