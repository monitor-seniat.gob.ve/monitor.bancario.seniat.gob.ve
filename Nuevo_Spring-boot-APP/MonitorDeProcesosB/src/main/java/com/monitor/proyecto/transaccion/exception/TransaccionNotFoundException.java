package com.monitor.proyecto.transaccion.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class TransaccionNotFoundException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public TransaccionNotFoundException(String mensaje) {
        super("No se encontro la Transaccion con id_transaccion: " + mensaje);
    }
}
