package com.monitor.proyecto.reportecierre.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ReporteCierreNotFoundException extends RuntimeException{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public ReporteCierreNotFoundException(String mensaje) {
        super("No se encontro el ReporteCierre con id_cierres: " + mensaje);
    }
}