package com.monitor.proyecto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonitorDeProcesosBApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonitorDeProcesosBApplication.class, args);
	}

}
