package com.monitor.proyecto.reportecierre.entity;

import java.sql.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "reportecierre", schema = "sistebank")
public class ReporteCierre {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_cierres;

	@Column(name = "numero_de_reportes", length = 100, nullable = false)
	private String numero_de_reportes;

	@Column(name = "total_por_reporte", nullable = false)
	private Long total_por_reporte;

	@Column(name = "fecha_reporte", nullable = false)
	private Date fecha_reporte;

	@Column(name = "nombre_reporte", length = 100, nullable = false)
	private String nombre_reporte;

	@Column(name = "total_aprobadas", length = 100, nullable = false)
	private String total_aprobadas;

	@Column(name = "total_reversadas", length = 10, nullable = false)
	private String total_reversadas;

	@Column(name = "id_reverso", nullable = false)
	private Long id_reverso;

	@Column(name = "id_distribucion", nullable = false)
	private Long id_distribucion;

	

	public ReporteCierre() {

	}



	public ReporteCierre(Long id_cierres, String numero_de_reportes, Long total_por_reporte, Date fecha_reporte,
			String nombre_reporte, String total_aprobadas, String total_reversadas, Long id_reverso,
			Long id_distribucion) {
		super();
		this.id_cierres = id_cierres;
		this.numero_de_reportes = numero_de_reportes;
		this.total_por_reporte = total_por_reporte;
		this.fecha_reporte = fecha_reporte;
		this.nombre_reporte = nombre_reporte;
		this.total_aprobadas = total_aprobadas;
		this.total_reversadas = total_reversadas;
		this.id_reverso = id_reverso;
		this.id_distribucion = id_distribucion;
	}



	public Long getId_cierres() {
		return id_cierres;
	}



	public void setId_cierres(Long id_cierres) {
		this.id_cierres = id_cierres;
	}



	public String getNumero_de_reportes() {
		return numero_de_reportes;
	}



	public void setNumero_de_reportes(String numero_de_reportes) {
		this.numero_de_reportes = numero_de_reportes;
	}



	public Long getTotal_por_reporte() {
		return total_por_reporte;
	}



	public void setTotal_por_reporte(Long total_por_reporte) {
		this.total_por_reporte = total_por_reporte;
	}



	public Date getFecha_reporte() {
		return fecha_reporte;
	}



	public void setFecha_reporte(Date fecha_reporte) {
		this.fecha_reporte = fecha_reporte;
	}



	public String getNombre_reporte() {
		return nombre_reporte;
	}



	public void setNombre_reporte(String nombre_reporte) {
		this.nombre_reporte = nombre_reporte;
	}



	public String getTotal_aprobadas() {
		return total_aprobadas;
	}



	public void setTotal_aprobadas(String total_aprobadas) {
		this.total_aprobadas = total_aprobadas;
	}



	public String getTotal_reversadas() {
		return total_reversadas;
	}



	public void setTotal_reversadas(String total_reversadas) {
		this.total_reversadas = total_reversadas;
	}



	public Long getId_reverso() {
		return id_reverso;
	}



	public void setId_reverso(Long id_reverso) {
		this.id_reverso = id_reverso;
	}



	public Long getId_distribucion() {
		return id_distribucion;
	}



	public void setId_distribucion(Long id_distribucion) {
		this.id_distribucion = id_distribucion;
	}


}
