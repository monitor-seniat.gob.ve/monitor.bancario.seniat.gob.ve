package com.monitor.proyecto.reportecierre.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.monitor.proyecto.reportecierre.entity.ReporteCierre;

public interface ReporteCierreRepository extends JpaRepository<ReporteCierre, Long>{

}
