package com.crud.login.form.InterFace;
import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;

import com.crud.login.form.modelo.Credenciales;

public interface ICrudLoginForm {

    List<Credenciales> listarTodasLosLogin();

    Credenciales agregarLogin(Credenciales loginForm);

    void borrarLogin(int id);

    Optional<Credenciales> obtenerLoginPorId(int id);

    ResponseEntity<String> login(Credenciales credenciales);
}
