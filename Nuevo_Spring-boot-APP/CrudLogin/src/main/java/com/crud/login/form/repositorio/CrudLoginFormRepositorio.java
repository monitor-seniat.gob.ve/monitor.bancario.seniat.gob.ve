package com.crud.login.form.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import com.crud.login.form.modelo.Credenciales;

public interface CrudLoginFormRepositorio extends JpaRepository<Credenciales, Integer> { 

	// Define los métodos personalizados si es necesario
}
