package com.crud.login.form.controlador;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crud.login.form.InterFace.ICrudLoginForm;
import com.crud.login.form.modelo.Credenciales;
import com.crud.login.form.repositorio.CrudLoginFormRepositorio;

@RestController
@RequestMapping("api/v1/")
@CrossOrigin(origins = "http://localhost:4200")
public class CrudLoginControlador implements ICrudLoginForm {

    @Autowired
    private CrudLoginFormRepositorio repositorio;

    // Operación GET para recuperar todas las credenciales de login
    @GetMapping("/Logins")
    public List<Credenciales> listarTodasLosLogin() {
        return repositorio.findAll();
    }

    // Operación POST para agregar una nueva credencial de login
    @PostMapping("/Logins")
    public Credenciales agregarLogin(@RequestBody Credenciales loginForm) {
        return repositorio.save(loginForm);
    }

    // Operación DELETE para eliminar una credencial de login por ID
    @DeleteMapping("/Logins/{id}")
    public void borrarLogin(@PathVariable int id) {
        repositorio.deleteById(id);
    }

    // Operación GET para recuperar una credencial de login por ID
    @GetMapping("/Logins/{id}")
    public Optional<Credenciales> obtenerLoginPorId(@PathVariable int id) {
        return repositorio.findById(id);
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody Credenciales credenciales) {
        // Lógica para verificar las credenciales y autenticar al usuario
        if (validarCredenciales(credenciales)) {
            // Credenciales válidas, devuelve un token de autenticación
            String token = generarTokenDeAutenticacionString(credenciales.getNombreUsuario());
            return new ResponseEntity<>(token, HttpStatus.OK);
        } else {
            // Credenciales inválidas, devuelve un mensaje de error
            return new ResponseEntity<>("Credenciales inválidas", HttpStatus.UNAUTHORIZED);
        }
    }

    private boolean validarCredenciales(Credenciales credenciales) {
        // Lógica para validar las credenciales (por ejemplo, consultar la base de datos)
        // Devuelve true si las credenciales son válidas, false si no lo son
    	return credenciales.getNombreUsuario().equals("usuario") && credenciales.getContrasena().equals("contrasena");
    
    }

    private String generarTokenDeAutenticacionString(String nombreUsuario) {
        // Lógica para generar un token de autenticación (por ejemplo, JWT)
        // Devuelve el token generado
    	
    	return "token_de_autenticacion_generado";
    }
}
