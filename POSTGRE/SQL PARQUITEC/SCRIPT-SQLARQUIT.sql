
CREATE SEQUENCE sistbank.distribucion_id_distribucion_seq_1;

CREATE TABLE sistbank.distribucion (
                id_distribucion INTEGER NOT NULL DEFAULT nextval('sistbank.distribucion_id_distribucion_seq_1'),
                fecha_distribucion DATE NOT NULL,
                monto_total_distribuido NCHAR(100) NOT NULL,
                id_cierres INTEGER NOT NULL,
                codigo_transaccion_distribuidas NCHAR(100) NOT NULL,
                CONSTRAINT id_distribucion_pk PRIMARY KEY (id_distribucion)
);


ALTER SEQUENCE sistbank.distribucion_id_distribucion_seq_1 OWNED BY sistbank.distribucion.id_distribucion;

CREATE SEQUENCE sistbank.modulo_usuario_id_usuario_seq;

CREATE TABLE sistbank.modulo_usuario (
                id_usuario INTEGER NOT NULL DEFAULT nextval('sistbank.modulo_usuario_id_usuario_seq'),
                perfil CHAR(10) NOT NULL,
                Nombre_Usuario CHAR(100) NOT NULL,
                pass_w CHAR(10) NOT NULL,
                CONSTRAINT id_usuario_pk PRIMARY KEY (id_usuario)
);


ALTER SEQUENCE sistbank.modulo_usuario_id_usuario_seq OWNED BY sistbank.modulo_usuario.id_usuario;

CREATE SEQUENCE sistbank.seguridad_de_acceso_al_usuario_id_seguridad_seq;

CREATE TABLE sistbank.seguridad_de_acceso_al_usuario (
                id_seguridad INTEGER NOT NULL DEFAULT nextval('sistbank.seguridad_de_acceso_al_usuario_id_seguridad_seq'),
                codigo_perfil_del_sistema INTEGER NOT NULL,
                descripcion_de_nivel CHAR(100) NOT NULL,
                id_usuario INTEGER NOT NULL,
                CONSTRAINT id_seguridad_pk PRIMARY KEY (id_seguridad)
);


ALTER SEQUENCE sistbank.seguridad_de_acceso_al_usuario_id_seguridad_seq OWNED BY sistbank.seguridad_de_acceso_al_usuario.id_seguridad;

CREATE SEQUENCE sistbank.mantenimiento_id_mantenimiento_seq_1;

CREATE TABLE sistbank.mantenimiento (
                id_mantenimiento INTEGER NOT NULL DEFAULT nextval('sistbank.mantenimiento_id_mantenimiento_seq_1'),
                tarea_en_backend CHAR(10) NOT NULL,
                frecuencia_de_mantenimiento DATE NOT NULL,
                estado_inactivo BOOLEAN NOT NULL,
                codigo_cambio_hecho INTEGER NOT NULL,
                id_seguridad INTEGER NOT NULL,
                CONSTRAINT id_mantenimiento_pk PRIMARY KEY (id_mantenimiento)
);


ALTER SEQUENCE sistbank.mantenimiento_id_mantenimiento_seq_1 OWNED BY sistbank.mantenimiento.id_mantenimiento;

CREATE SEQUENCE sistbank.alertas_por_vista_id_alerta_seq_1;

CREATE TABLE sistbank.alertas_por_vista (
                id_alerta INTEGER NOT NULL DEFAULT nextval('sistbank.alertas_por_vista_id_alerta_seq_1'),
                tiempo_conexion DATE NOT NULL,
                id_mantenimiento INTEGER NOT NULL,
                codigo_estado_alerta INTEGER NOT NULL,
                codigo_registro_alertados INTEGER NOT NULL,
                tipo_de_estado_alertados NCHAR(10) NOT NULL,
                descripcion_por_situacion CHAR(100) NOT NULL,
                id_reporte INTEGER NOT NULL,
                CONSTRAINT id_alerta_pk PRIMARY KEY (id_alerta)
);


ALTER SEQUENCE sistbank.alertas_por_vista_id_alerta_seq_1 OWNED BY sistbank.alertas_por_vista.id_alerta;

CREATE SEQUENCE sistbank.registro_de_reportes_id_reporte_seq;

CREATE TABLE sistbank.registro_de_reportes (
                id_reporte INTEGER NOT NULL DEFAULT nextval('sistbank.registro_de_reportes_id_reporte_seq'),
                codigo_por_tipo_reporte NCHAR(100) NOT NULL,
                descripcion_del_reporte CHAR(100) NOT NULL,
                fecha_creacion DATE NOT NULL,
                nombre_usuario_administrador CHAR(10) NOT NULL,
                fecha_modificacion DATE NOT NULL,
                nombre_usuario_modificador CHAR(10) NOT NULL,
                total_reportes_registrados NCHAR(100) NOT NULL,
                id_distribucion INTEGER NOT NULL,
                id_alerta INTEGER NOT NULL,
                CONSTRAINT id_reporte_pk PRIMARY KEY (id_reporte)
);


ALTER SEQUENCE sistbank.registro_de_reportes_id_reporte_seq OWNED BY sistbank.registro_de_reportes.id_reporte;

CREATE SEQUENCE sistbank.transaccion_id_transaccion_seq;

CREATE TABLE sistbank.transaccion (
                id_transaccion INTEGER NOT NULL DEFAULT nextval('sistbank.transaccion_id_transaccion_seq'),
                banco VARCHAR(200) NOT NULL,
                rif CHAR(10) NOT NULL,
                periodo DATE NOT NULL,
                monto CHAR(10) NOT NULL,
                transacciones_procesadas NCHAR(100) NOT NULL,
                nro_documento CHAR(10) NOT NULL,
                forma_o_documento CHAR(10) NOT NULL,
                id_usuario INTEGER NOT NULL,
                agencia CHAR(10) NOT NULL,
                tipo_transaccion CHAR(10) NOT NULL,
                fecha_transaccion DATE NOT NULL,
                CONSTRAINT id_transaccion_pk PRIMARY KEY (id_transaccion)
);


ALTER SEQUENCE sistbank.transaccion_id_transaccion_seq OWNED BY sistbank.transaccion.id_transaccion;

CREATE SEQUENCE sistbank.monitor_de_reversos_tecnicos_id_reverso_seq;

CREATE TABLE sistbank.monitor_de_reversos_tecnicos (
                id_reverso INTEGER NOT NULL DEFAULT nextval('sistbank.monitor_de_reversos_tecnicos_id_reverso_seq'),
                id_transaccion INTEGER NOT NULL,
                tipo_error CHAR(100) NOT NULL,
                fecha_reverso DATE NOT NULL,
                codigo_reverso NCHAR(100) NOT NULL,
                codigo_no_reversada NCHAR(100) NOT NULL,
                CONSTRAINT id_reverso_pk PRIMARY KEY (id_reverso)
);


ALTER SEQUENCE sistbank.monitor_de_reversos_tecnicos_id_reverso_seq OWNED BY sistbank.monitor_de_reversos_tecnicos.id_reverso;

CREATE SEQUENCE sistbank.reporte_de_cierres_id_cierres_seq;

CREATE TABLE sistbank.reporte_de_cierres (
                id_cierres INTEGER NOT NULL DEFAULT nextval('sistbank.reporte_de_cierres_id_cierres_seq'),
                numero_de_reportes CHAR(100) NOT NULL,
                total_por_reporte INTEGER NOT NULL,
                fecha_reporte DATE NOT NULL,
                nombre_reporte CHAR(100) NOT NULL,
                total_aprobadas NCHAR(100) NOT NULL,
                total_reversadas NCHAR(100) NOT NULL,
                id_reverso INTEGER NOT NULL,
                id_distribucion INTEGER NOT NULL,
                CONSTRAINT id_cierres_pk PRIMARY KEY (id_cierres)
);


ALTER SEQUENCE sistbank.reporte_de_cierres_id_cierres_seq OWNED BY sistbank.reporte_de_cierres.id_cierres;

ALTER TABLE sistbank.reporte_de_cierres ADD CONSTRAINT distribucion_reporte_de_cierres_fk
FOREIGN KEY (id_distribucion)
REFERENCES sistbank.distribucion (id_distribucion)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE sistbank.registro_de_reportes ADD CONSTRAINT distribucion_registro_de_reportes_fk
FOREIGN KEY (id_distribucion)
REFERENCES sistbank.distribucion (id_distribucion)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE sistbank.transaccion ADD CONSTRAINT modulo_usuario_transaccion_fk
FOREIGN KEY (id_usuario)
REFERENCES sistbank.modulo_usuario (id_usuario)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE sistbank.seguridad_de_acceso_al_usuario ADD CONSTRAINT modulo_usuario_seguridad_de_acceso_al_usuario_fk
FOREIGN KEY (id_usuario)
REFERENCES sistbank.modulo_usuario (id_usuario)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE sistbank.mantenimiento ADD CONSTRAINT seguridad_de_acceso_al_usuario_mantenimiento_fk
FOREIGN KEY (id_seguridad)
REFERENCES sistbank.seguridad_de_acceso_al_usuario (id_seguridad)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE sistbank.alertas_por_vista ADD CONSTRAINT mantenimiento_alertas_por_vista_fk
FOREIGN KEY (id_mantenimiento)
REFERENCES sistbank.mantenimiento (id_mantenimiento)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE sistbank.registro_de_reportes ADD CONSTRAINT alertas_por_vista_registro_de_reportes_fk
FOREIGN KEY (id_alerta)
REFERENCES sistbank.alertas_por_vista (id_alerta)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE sistbank.monitor_de_reversos_tecnicos ADD CONSTRAINT transaccion_monitor_de_reversos_tecnicos_fk
FOREIGN KEY (id_transaccion)
REFERENCES sistbank.transaccion (id_transaccion)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE sistbank.reporte_de_cierres ADD CONSTRAINT monitor_de_reversos_tecnicos_reporte_de_cierres_fk
FOREIGN KEY (id_reverso)
REFERENCES sistbank.monitor_de_reversos_tecnicos (id_reverso)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
