-- Crear esquema 19-02-2024 -09-12-pm

 CREATE SCHEMA sistbank;
 
CREATE TABLE sistbank.modulo_usuario (
    id_usuario integer NOT NULL,
    perfil character(10)  NOT NULL,
    Nombre_Usuario character(10) NOT NULL,
	pass_w character (10) NOT NULL,
    CONSTRAINT modulo_usuario_pkey PRIMARY KEY (id_usuario)
    
);

CREATE TABLE sistbank.transaccion (
    id_transaccion integer NOT NULL,
    banco character(10)  NOT NULL,
    rif character(10)  NOT NULL,
    periodo date NOT NULL,
    monto character(10) NOT NULL,
    codigo_procesada character(10)  NOT NULL,
    nro_documento character(10) NOT NULL,
    forma_o_documento character(10) NOT NULL,
    id_usuario integer NOT NULL,
    agencia character(10)  NOT NULL,
    tipo_transaccion text  NOT NULL,
    fecha_transaccion date NOT NULL,
	CONSTRAINT transaccion_pkey PRIMARY KEY (id_transaccion)
  -- FOREIGN KEY (id_usuario) REFERENCES sistbank.modulo_usuario (id_usuario)
);


CREATE TABLE sistbank.reporte_de_cierres (
   id_cierres integer NOT NULL,
    codigo_reportes_de_cierres integer NOT NULL,
    transaccion_por_reporte character(100)  NOT NULL,
    fecha_reporte date NOT NULL,
    nombre_reporte text NOT NULL,
    total_aprobadas integer NOT NULL,
    total_reversadas integer NOT NULL,
	CONSTRAINT reporte_de_cierres_pkey PRIMARY KEY (id_cierres)
  --FOREIGN KEY (id_usuario) REFERENCES sistbank.modulo_usuario (id_usuario)
);


CREATE TABLE sistbank.monitor_de_reversos_tecnicos (
   id_reverso integer NOT NULL,
    id_transaccion integer,
    tipo_error text NOT NULL,
    fecha_reverso date NOT NULL,
    codigo_reverso character(100),
    codigo_no_reversada character(100),
	CONSTRAINT monitor_de_reversos_tecnicos_pkey PRIMARY KEY (id_reverso)
  --FOREIGN KEY (id_reporte_distribucion) REFERENCES sistbank.reporte_de_cierres (id_reporte_distribucion)
);

CREATE TABLE sistbank.distribucion (
    id_distribucion integer NOT NULL,
    fecha_distribucion date NOT NULL,
    id_cierres integer NOT NULL,
    monto_total_distribuido numeric(10, 2) NOT NULL,
    numero_transacciones_distribuidas integer NOT NULL,
	CONSTRAINT distribucion_pkey PRIMARY KEY (id_distribucion)
  --FOREIGN KEY (id_reporte_distribucion) REFERENCES sistbank.registro_de_reportes (id_reporte_de_registro)
);

CREATE TABLE sistbank.registro_de_reportes (
	id_reporte_de_registro integer NOT NULL,
    codigo_por_tipo_reporte char(100),
    descripcion text  NOT NULL,
    fecha_creacion date NOT NULL,
    usuario_administrador text  NOT NULL,
    fecha_modificacion timestamp without time zone NOT NULL,
    usuario_modificador text  NOT NULL,
    id_reporte_distribucion integer NOT NULL,
	total_reportes_registrados char(100),
	id_distribucion integer NOT NULL,
	CONSTRAINT registro_de_reportes_pkey PRIMARY KEY (id_reporte_de_registro)
  --FOREIGN KEY (id_reporte_distribucion) REFERENCES sistbank.reporte_de_cierres (id_reporte_distribucion)
);

CREATE TABLE sistbank.alertas_por_vista (
   	id_alerta integer NOT NULL,
    tiempo_conexion date NOT NULL,
    codigo_estado_alerta integer NOT NULL,
    codigo_registro_alertados integer NOT NULL,
    tipo_de_estado_alertados integer,
    descripcion_por_situacion character(100),
	CONSTRAINT alertas_por_vista_pkey PRIMARY KEY (id_alerta)
  --FOREIGN KEY (id_reporte_de_registro) REFERENCES sistbank.registro_de_reportes (id_reporte_de_registro)
);

CREATE TABLE sistbank.mantenimiento (
    id_mantenimiento integer NOT NULL,
    tarea_en_backend text NOT NULL,
    frecuencia_de_mantenimiento date NOT NULL,
    id_usuario_adm integer,
    estado_inactivo boolean,
    codigo_cambio_hecho integer,
	CONSTRAINT mantenimiento_pkey PRIMARY KEY (id_mantenimiento)
);

CREATE TABLE sistbank.seguridad_de_acceso_al_usuario ( 
	id_usuario_seguridad_acceso integer NOT NULL,
    Descripcion_de_nivel text  NOT NULL,
    codigo_perfil_del_sistema integer NOT NULL,
    id_usuario integer NOT NULL,
	CONSTRAINT seguridad_de_acceso_al_usuario_pkey PRIMARY KEY (id_usuario_seguridad_acceso)
  --FOREIGN KEY (id_usuario) REFERENCES sistbank.modulo_usuario (id_usuario)
);

