PGDMP     -    +                |            SistemaTrxPagos    15.5    15.5 D    K           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            L           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            M           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            N           1262    57599    SistemaTrxPagos    DATABASE     �   CREATE DATABASE "SistemaTrxPagos" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'Spanish_Spain.1252';
 !   DROP DATABASE "SistemaTrxPagos";
                estandar    false                        2615    57600    sistbank    SCHEMA        CREATE SCHEMA sistbank;
    DROP SCHEMA sistbank;
                estandar    false            �            1259    57743    alertas_por_vista    TABLE     ~  CREATE TABLE sistbank.alertas_por_vista (
    id_alerta integer NOT NULL,
    tiempo_conexion date NOT NULL,
    id_mantenimiento integer NOT NULL,
    codigo_estado_alerta integer NOT NULL,
    codigo_registro_alertados integer NOT NULL,
    tipo_de_estado_alertados character(10) NOT NULL,
    descripcion_por_situacion character(100) NOT NULL,
    id_reporte integer NOT NULL
);
 '   DROP TABLE sistbank.alertas_por_vista;
       sistbank         heap    estandar    false    6            �            1259    57742 !   alertas_por_vista_id_alerta_seq_1    SEQUENCE     �   CREATE SEQUENCE sistbank.alertas_por_vista_id_alerta_seq_1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 :   DROP SEQUENCE sistbank.alertas_por_vista_id_alerta_seq_1;
       sistbank          estandar    false    6    224            O           0    0 !   alertas_por_vista_id_alerta_seq_1    SEQUENCE OWNED BY     i   ALTER SEQUENCE sistbank.alertas_por_vista_id_alerta_seq_1 OWNED BY sistbank.alertas_por_vista.id_alerta;
          sistbank          estandar    false    223            �            1259    57715    distribucion    TABLE       CREATE TABLE sistbank.distribucion (
    id_distribucion integer NOT NULL,
    fecha_distribucion date NOT NULL,
    monto_total_distribuido character(100) NOT NULL,
    id_cierres integer NOT NULL,
    codigo_transaccion_distribuidas character(100) NOT NULL
);
 "   DROP TABLE sistbank.distribucion;
       sistbank         heap    estandar    false    6            �            1259    57714 "   distribucion_id_distribucion_seq_1    SEQUENCE     �   CREATE SEQUENCE sistbank.distribucion_id_distribucion_seq_1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE sistbank.distribucion_id_distribucion_seq_1;
       sistbank          estandar    false    6    216            P           0    0 "   distribucion_id_distribucion_seq_1    SEQUENCE OWNED BY     k   ALTER SEQUENCE sistbank.distribucion_id_distribucion_seq_1 OWNED BY sistbank.distribucion.id_distribucion;
          sistbank          estandar    false    215            �            1259    57736    mantenimiento    TABLE     *  CREATE TABLE sistbank.mantenimiento (
    id_mantenimiento integer NOT NULL,
    tarea_en_backend character(20) NOT NULL,
    frecuencia_de_mantenimiento date NOT NULL,
    estado_inactivo character(20) NOT NULL,
    codigo_cambio_hecho numeric(10,0) NOT NULL,
    id_seguridad integer NOT NULL
);
 #   DROP TABLE sistbank.mantenimiento;
       sistbank         heap    estandar    false    6            �            1259    57735 $   mantenimiento_id_mantenimiento_seq_1    SEQUENCE     �   CREATE SEQUENCE sistbank.mantenimiento_id_mantenimiento_seq_1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE sistbank.mantenimiento_id_mantenimiento_seq_1;
       sistbank          estandar    false    6    222            Q           0    0 $   mantenimiento_id_mantenimiento_seq_1    SEQUENCE OWNED BY     o   ALTER SEQUENCE sistbank.mantenimiento_id_mantenimiento_seq_1 OWNED BY sistbank.mantenimiento.id_mantenimiento;
          sistbank          estandar    false    221            �            1259    57722    modulo_usuario    TABLE     �   CREATE TABLE sistbank.modulo_usuario (
    id_usuario integer NOT NULL,
    perfil character(10) NOT NULL,
    nombre_usuario character(100) NOT NULL,
    pass_w character(10) NOT NULL
);
 $   DROP TABLE sistbank.modulo_usuario;
       sistbank         heap    estandar    false    6            �            1259    57721    modulo_usuario_id_usuario_seq    SEQUENCE     �   CREATE SEQUENCE sistbank.modulo_usuario_id_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE sistbank.modulo_usuario_id_usuario_seq;
       sistbank          estandar    false    218    6            R           0    0    modulo_usuario_id_usuario_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE sistbank.modulo_usuario_id_usuario_seq OWNED BY sistbank.modulo_usuario.id_usuario;
          sistbank          estandar    false    217            �            1259    57764    monitor_de_reversos_tecnicos    TABLE     #  CREATE TABLE sistbank.monitor_de_reversos_tecnicos (
    id_reverso integer NOT NULL,
    id_transaccion integer NOT NULL,
    tipo_error character(100) NOT NULL,
    fecha_reverso date NOT NULL,
    codigo_reverso character(100) NOT NULL,
    codigo_no_reversada character(100) NOT NULL
);
 2   DROP TABLE sistbank.monitor_de_reversos_tecnicos;
       sistbank         heap    estandar    false    6            �            1259    57763 +   monitor_de_reversos_tecnicos_id_reverso_seq    SEQUENCE     �   CREATE SEQUENCE sistbank.monitor_de_reversos_tecnicos_id_reverso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE sistbank.monitor_de_reversos_tecnicos_id_reverso_seq;
       sistbank          estandar    false    6    230            S           0    0 +   monitor_de_reversos_tecnicos_id_reverso_seq    SEQUENCE OWNED BY        ALTER SEQUENCE sistbank.monitor_de_reversos_tecnicos_id_reverso_seq OWNED BY sistbank.monitor_de_reversos_tecnicos.id_reverso;
          sistbank          estandar    false    229            �            1259    57750    registro_de_reportes    TABLE     �  CREATE TABLE sistbank.registro_de_reportes (
    id_reporte integer NOT NULL,
    codigo_por_tipo_reporte character(100) NOT NULL,
    descripcion_del_reporte character(100) NOT NULL,
    fecha_creacion date NOT NULL,
    nombre_usuario_administrador character(10) NOT NULL,
    fecha_modificacion date NOT NULL,
    nombre_usuario_modificador character(10) NOT NULL,
    total_reportes_registrados character(100) NOT NULL,
    id_distribucion integer NOT NULL,
    id_alerta integer NOT NULL
);
 *   DROP TABLE sistbank.registro_de_reportes;
       sistbank         heap    estandar    false    6            �            1259    57749 #   registro_de_reportes_id_reporte_seq    SEQUENCE     �   CREATE SEQUENCE sistbank.registro_de_reportes_id_reporte_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE sistbank.registro_de_reportes_id_reporte_seq;
       sistbank          estandar    false    6    226            T           0    0 #   registro_de_reportes_id_reporte_seq    SEQUENCE OWNED BY     o   ALTER SEQUENCE sistbank.registro_de_reportes_id_reporte_seq OWNED BY sistbank.registro_de_reportes.id_reporte;
          sistbank          estandar    false    225            �            1259    57771    reporte_de_cierres    TABLE     �  CREATE TABLE sistbank.reporte_de_cierres (
    id_cierres integer NOT NULL,
    numero_de_reportes character(100) NOT NULL,
    total_por_reporte integer NOT NULL,
    fecha_reporte date NOT NULL,
    nombre_reporte character(100) NOT NULL,
    total_aprobadas character(100) NOT NULL,
    total_reversadas character(100) NOT NULL,
    id_reverso integer NOT NULL,
    id_distribucion integer NOT NULL
);
 (   DROP TABLE sistbank.reporte_de_cierres;
       sistbank         heap    estandar    false    6            �            1259    57770 !   reporte_de_cierres_id_cierres_seq    SEQUENCE     �   CREATE SEQUENCE sistbank.reporte_de_cierres_id_cierres_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 :   DROP SEQUENCE sistbank.reporte_de_cierres_id_cierres_seq;
       sistbank          estandar    false    232    6            U           0    0 !   reporte_de_cierres_id_cierres_seq    SEQUENCE OWNED BY     k   ALTER SEQUENCE sistbank.reporte_de_cierres_id_cierres_seq OWNED BY sistbank.reporte_de_cierres.id_cierres;
          sistbank          estandar    false    231            �            1259    57729    seguridad_de_acceso_al_usuario    TABLE     �   CREATE TABLE sistbank.seguridad_de_acceso_al_usuario (
    id_seguridad integer NOT NULL,
    codigo_perfil_del_sistema integer NOT NULL,
    descripcion_de_nivel character(100) NOT NULL,
    id_usuario integer NOT NULL
);
 4   DROP TABLE sistbank.seguridad_de_acceso_al_usuario;
       sistbank         heap    estandar    false    6            �            1259    57728 /   seguridad_de_acceso_al_usuario_id_seguridad_seq    SEQUENCE     �   CREATE SEQUENCE sistbank.seguridad_de_acceso_al_usuario_id_seguridad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 H   DROP SEQUENCE sistbank.seguridad_de_acceso_al_usuario_id_seguridad_seq;
       sistbank          estandar    false    6    220            V           0    0 /   seguridad_de_acceso_al_usuario_id_seguridad_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE sistbank.seguridad_de_acceso_al_usuario_id_seguridad_seq OWNED BY sistbank.seguridad_de_acceso_al_usuario.id_seguridad;
          sistbank          estandar    false    219            �            1259    57757    transaccion    TABLE     �  CREATE TABLE sistbank.transaccion (
    id_transaccion integer NOT NULL,
    banco character varying(200) NOT NULL,
    rif character(10) NOT NULL,
    periodo date NOT NULL,
    monto character(10) NOT NULL,
    transacciones_procesadas character(100) NOT NULL,
    nro_documento character(10) NOT NULL,
    forma_o_documento character(10) NOT NULL,
    id_usuario integer NOT NULL,
    agencia character(10) NOT NULL,
    tipo_transaccion character(10) NOT NULL,
    fecha_transaccion date NOT NULL
);
 !   DROP TABLE sistbank.transaccion;
       sistbank         heap    estandar    false    6            �            1259    57756    transaccion_id_transaccion_seq    SEQUENCE     �   CREATE SEQUENCE sistbank.transaccion_id_transaccion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE sistbank.transaccion_id_transaccion_seq;
       sistbank          estandar    false    228    6            W           0    0    transaccion_id_transaccion_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE sistbank.transaccion_id_transaccion_seq OWNED BY sistbank.transaccion.id_transaccion;
          sistbank          estandar    false    227            �           2604    57746    alertas_por_vista id_alerta    DEFAULT     �   ALTER TABLE ONLY sistbank.alertas_por_vista ALTER COLUMN id_alerta SET DEFAULT nextval('sistbank.alertas_por_vista_id_alerta_seq_1'::regclass);
 L   ALTER TABLE sistbank.alertas_por_vista ALTER COLUMN id_alerta DROP DEFAULT;
       sistbank          estandar    false    223    224    224            �           2604    57718    distribucion id_distribucion    DEFAULT     �   ALTER TABLE ONLY sistbank.distribucion ALTER COLUMN id_distribucion SET DEFAULT nextval('sistbank.distribucion_id_distribucion_seq_1'::regclass);
 M   ALTER TABLE sistbank.distribucion ALTER COLUMN id_distribucion DROP DEFAULT;
       sistbank          estandar    false    215    216    216            �           2604    57739    mantenimiento id_mantenimiento    DEFAULT     �   ALTER TABLE ONLY sistbank.mantenimiento ALTER COLUMN id_mantenimiento SET DEFAULT nextval('sistbank.mantenimiento_id_mantenimiento_seq_1'::regclass);
 O   ALTER TABLE sistbank.mantenimiento ALTER COLUMN id_mantenimiento DROP DEFAULT;
       sistbank          estandar    false    221    222    222            �           2604    57725    modulo_usuario id_usuario    DEFAULT     �   ALTER TABLE ONLY sistbank.modulo_usuario ALTER COLUMN id_usuario SET DEFAULT nextval('sistbank.modulo_usuario_id_usuario_seq'::regclass);
 J   ALTER TABLE sistbank.modulo_usuario ALTER COLUMN id_usuario DROP DEFAULT;
       sistbank          estandar    false    218    217    218            �           2604    57767 '   monitor_de_reversos_tecnicos id_reverso    DEFAULT     �   ALTER TABLE ONLY sistbank.monitor_de_reversos_tecnicos ALTER COLUMN id_reverso SET DEFAULT nextval('sistbank.monitor_de_reversos_tecnicos_id_reverso_seq'::regclass);
 X   ALTER TABLE sistbank.monitor_de_reversos_tecnicos ALTER COLUMN id_reverso DROP DEFAULT;
       sistbank          estandar    false    230    229    230            �           2604    57753    registro_de_reportes id_reporte    DEFAULT     �   ALTER TABLE ONLY sistbank.registro_de_reportes ALTER COLUMN id_reporte SET DEFAULT nextval('sistbank.registro_de_reportes_id_reporte_seq'::regclass);
 P   ALTER TABLE sistbank.registro_de_reportes ALTER COLUMN id_reporte DROP DEFAULT;
       sistbank          estandar    false    225    226    226            �           2604    57774    reporte_de_cierres id_cierres    DEFAULT     �   ALTER TABLE ONLY sistbank.reporte_de_cierres ALTER COLUMN id_cierres SET DEFAULT nextval('sistbank.reporte_de_cierres_id_cierres_seq'::regclass);
 N   ALTER TABLE sistbank.reporte_de_cierres ALTER COLUMN id_cierres DROP DEFAULT;
       sistbank          estandar    false    232    231    232            �           2604    57732 +   seguridad_de_acceso_al_usuario id_seguridad    DEFAULT     �   ALTER TABLE ONLY sistbank.seguridad_de_acceso_al_usuario ALTER COLUMN id_seguridad SET DEFAULT nextval('sistbank.seguridad_de_acceso_al_usuario_id_seguridad_seq'::regclass);
 \   ALTER TABLE sistbank.seguridad_de_acceso_al_usuario ALTER COLUMN id_seguridad DROP DEFAULT;
       sistbank          estandar    false    220    219    220            �           2604    57760    transaccion id_transaccion    DEFAULT     �   ALTER TABLE ONLY sistbank.transaccion ALTER COLUMN id_transaccion SET DEFAULT nextval('sistbank.transaccion_id_transaccion_seq'::regclass);
 K   ALTER TABLE sistbank.transaccion ALTER COLUMN id_transaccion DROP DEFAULT;
       sistbank          estandar    false    227    228    228            @          0    57743    alertas_por_vista 
   TABLE DATA                 sistbank          estandar    false    224   =W       8          0    57715    distribucion 
   TABLE DATA                 sistbank          estandar    false    216   
X       >          0    57736    mantenimiento 
   TABLE DATA                 sistbank          estandar    false    222   �X       :          0    57722    modulo_usuario 
   TABLE DATA                 sistbank          estandar    false    218   gY       F          0    57764    monitor_de_reversos_tecnicos 
   TABLE DATA                 sistbank          estandar    false    230   �Y       B          0    57750    registro_de_reportes 
   TABLE DATA                 sistbank          estandar    false    226   �Z       H          0    57771    reporte_de_cierres 
   TABLE DATA                 sistbank          estandar    false    232   �[       <          0    57729    seguridad_de_acceso_al_usuario 
   TABLE DATA                 sistbank          estandar    false    220   e\       D          0    57757    transaccion 
   TABLE DATA                 sistbank          estandar    false    228   \       X           0    0 !   alertas_por_vista_id_alerta_seq_1    SEQUENCE SET     R   SELECT pg_catalog.setval('sistbank.alertas_por_vista_id_alerta_seq_1', 1, false);
          sistbank          estandar    false    223            Y           0    0 "   distribucion_id_distribucion_seq_1    SEQUENCE SET     S   SELECT pg_catalog.setval('sistbank.distribucion_id_distribucion_seq_1', 1, false);
          sistbank          estandar    false    215            Z           0    0 $   mantenimiento_id_mantenimiento_seq_1    SEQUENCE SET     U   SELECT pg_catalog.setval('sistbank.mantenimiento_id_mantenimiento_seq_1', 1, false);
          sistbank          estandar    false    221            [           0    0    modulo_usuario_id_usuario_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('sistbank.modulo_usuario_id_usuario_seq', 1, false);
          sistbank          estandar    false    217            \           0    0 +   monitor_de_reversos_tecnicos_id_reverso_seq    SEQUENCE SET     \   SELECT pg_catalog.setval('sistbank.monitor_de_reversos_tecnicos_id_reverso_seq', 1, false);
          sistbank          estandar    false    229            ]           0    0 #   registro_de_reportes_id_reporte_seq    SEQUENCE SET     T   SELECT pg_catalog.setval('sistbank.registro_de_reportes_id_reporte_seq', 1, false);
          sistbank          estandar    false    225            ^           0    0 !   reporte_de_cierres_id_cierres_seq    SEQUENCE SET     R   SELECT pg_catalog.setval('sistbank.reporte_de_cierres_id_cierres_seq', 1, false);
          sistbank          estandar    false    231            _           0    0 /   seguridad_de_acceso_al_usuario_id_seguridad_seq    SEQUENCE SET     `   SELECT pg_catalog.setval('sistbank.seguridad_de_acceso_al_usuario_id_seguridad_seq', 1, false);
          sistbank          estandar    false    219            `           0    0    transaccion_id_transaccion_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('sistbank.transaccion_id_transaccion_seq', 1, false);
          sistbank          estandar    false    227            �           2606    57748    alertas_por_vista id_alerta_pk 
   CONSTRAINT     e   ALTER TABLE ONLY sistbank.alertas_por_vista
    ADD CONSTRAINT id_alerta_pk PRIMARY KEY (id_alerta);
 J   ALTER TABLE ONLY sistbank.alertas_por_vista DROP CONSTRAINT id_alerta_pk;
       sistbank            estandar    false    224            �           2606    57776     reporte_de_cierres id_cierres_pk 
   CONSTRAINT     h   ALTER TABLE ONLY sistbank.reporte_de_cierres
    ADD CONSTRAINT id_cierres_pk PRIMARY KEY (id_cierres);
 L   ALTER TABLE ONLY sistbank.reporte_de_cierres DROP CONSTRAINT id_cierres_pk;
       sistbank            estandar    false    232            �           2606    57720    distribucion id_distribucion_pk 
   CONSTRAINT     l   ALTER TABLE ONLY sistbank.distribucion
    ADD CONSTRAINT id_distribucion_pk PRIMARY KEY (id_distribucion);
 K   ALTER TABLE ONLY sistbank.distribucion DROP CONSTRAINT id_distribucion_pk;
       sistbank            estandar    false    216            �           2606    57741 !   mantenimiento id_mantenimiento_pk 
   CONSTRAINT     o   ALTER TABLE ONLY sistbank.mantenimiento
    ADD CONSTRAINT id_mantenimiento_pk PRIMARY KEY (id_mantenimiento);
 M   ALTER TABLE ONLY sistbank.mantenimiento DROP CONSTRAINT id_mantenimiento_pk;
       sistbank            estandar    false    222            �           2606    57755 "   registro_de_reportes id_reporte_pk 
   CONSTRAINT     j   ALTER TABLE ONLY sistbank.registro_de_reportes
    ADD CONSTRAINT id_reporte_pk PRIMARY KEY (id_reporte);
 N   ALTER TABLE ONLY sistbank.registro_de_reportes DROP CONSTRAINT id_reporte_pk;
       sistbank            estandar    false    226            �           2606    57769 *   monitor_de_reversos_tecnicos id_reverso_pk 
   CONSTRAINT     r   ALTER TABLE ONLY sistbank.monitor_de_reversos_tecnicos
    ADD CONSTRAINT id_reverso_pk PRIMARY KEY (id_reverso);
 V   ALTER TABLE ONLY sistbank.monitor_de_reversos_tecnicos DROP CONSTRAINT id_reverso_pk;
       sistbank            estandar    false    230            �           2606    57734 .   seguridad_de_acceso_al_usuario id_seguridad_pk 
   CONSTRAINT     x   ALTER TABLE ONLY sistbank.seguridad_de_acceso_al_usuario
    ADD CONSTRAINT id_seguridad_pk PRIMARY KEY (id_seguridad);
 Z   ALTER TABLE ONLY sistbank.seguridad_de_acceso_al_usuario DROP CONSTRAINT id_seguridad_pk;
       sistbank            estandar    false    220            �           2606    57762    transaccion id_transaccion_pk 
   CONSTRAINT     i   ALTER TABLE ONLY sistbank.transaccion
    ADD CONSTRAINT id_transaccion_pk PRIMARY KEY (id_transaccion);
 I   ALTER TABLE ONLY sistbank.transaccion DROP CONSTRAINT id_transaccion_pk;
       sistbank            estandar    false    228            �           2606    57727    modulo_usuario id_usuario_pk 
   CONSTRAINT     d   ALTER TABLE ONLY sistbank.modulo_usuario
    ADD CONSTRAINT id_usuario_pk PRIMARY KEY (id_usuario);
 H   ALTER TABLE ONLY sistbank.modulo_usuario DROP CONSTRAINT id_usuario_pk;
       sistbank            estandar    false    218            @   �   x����
1�����U��͓�(�]Km��fh���ƥ�`9|�_��|���j��D)m8��c����M����yS����8x'
��ِ1PG2+p���E��+�Ј'�{NOOq���x}<&�w���(_�+��e�a��ϷЌԺՓa��Z�
�e�	�\�� 7s���y��UU= ��g      8   �   x���v
Q���W(�,.IJ���K2�2�J�3��42S�t�R�3��r��J��K�Ks�2�)�:
@�ə�EE��:
��)��@UE�yŉ� �Hj�5�}B]�4uԍ�Lt�t��ԁ<3��=`�io����5 &EV      >   �   x�]���@D{�b; AhgeAAb0��,w+l���޳0'����LY�ť��j�0��Z�a;�8��Y�بI��D�HT�z 1	�'��fT���4;4V��v��@[ÝUǖ��I�����eb�&���t-j��B����_~
=��|�Iw�>�[�/��Co��J�      :   �   x���v
Q���W(�,.IJ�����O)�ɏ/-.M,��W��L��u
R��2st��s��R������5�}B]�4u�Sr @�K�*�O)�L/M�r(N��L,�K�O�+KU� �chdl�iZsqq �[8�      F   �   x���1�0�������T����С lu1I�!��K��[���l�����8@���ń��A�v^�y�u�6���y0��&Dc-R��I�3����Ւ�+��0Oƙλ��顨L����|�(�6�T�R���^�Sn�,{��cd      B   �   x����N�0��}
߶Ij#8q��$4$6�Fn�������qev9����s���7���/�8���;��mU|$�4�fJ��8��D>���3r�RPKo�O�R�@�pb%��5J~L#*���q?I1��՝YZ�o��>K�_Y�?0�J��㔸���H3�����m��e]¢rp�0O+��?x\��ו[;7%�5V�eՍ�;{l���(�/�|�8      H   �   x�Ő��0��<Eo`�n�<p 1��z%��(�)�緊�hܩ������2�����������+�Ǫ���Ȍ=D�y�1��C�G�uS<O^_*Qo/�듞���0Nz|��LF7��xC�GG���g�Ȁl�P[r38�և��(�!���	cx�T��y��J��ĝ�� ]Mγ���P��O�|d:[ApЇ0      <   
   x���          D   �   x�����0�w������_����DЕ��hc���	���w�r�ey��J���V������j�B(����0�����A'���QK�w21J[u���X�e�U5���/cC���[����2���B�{�:��)�8]��?����A��<�/W�� F�1_�c>��[�������X�;�}�~�vA<�kF     