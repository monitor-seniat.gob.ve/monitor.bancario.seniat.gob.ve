import { TestBed } from '@angular/core/testing';

import { SeguridadusuarioService } from './seguridadusuario.service';

describe('SeguridadusuarioService', () => {
  let service: SeguridadusuarioService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SeguridadusuarioService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
