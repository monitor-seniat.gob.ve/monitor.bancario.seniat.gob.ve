import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeguridadusuarioComponent } from './seguridadusuario.component';

describe('SeguridadusuarioComponent', () => {
  let component: SeguridadusuarioComponent;
  let fixture: ComponentFixture<SeguridadusuarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SeguridadusuarioComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SeguridadusuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
