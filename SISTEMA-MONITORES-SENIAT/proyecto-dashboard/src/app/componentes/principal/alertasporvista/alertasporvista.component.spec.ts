import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertasporvistaComponent } from './alertasporvista.component';

describe('AlertasporvistaComponent', () => {
  let component: AlertasporvistaComponent;
  let fixture: ComponentFixture<AlertasporvistaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AlertasporvistaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AlertasporvistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
