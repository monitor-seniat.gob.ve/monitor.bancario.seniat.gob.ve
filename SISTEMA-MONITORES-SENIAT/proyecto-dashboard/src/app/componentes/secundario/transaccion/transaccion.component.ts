import { Component, OnInit } from '@angular/core';
import { Transaccion } from '../transaccion';

import { HttpErrorResponse } from '@angular/common/http';
import { TransaccionService } from '../listado-servicios/transaccion-servicio/transaccion.service';

@Component({
  selector: 'app-transaccion',
  templateUrl: './transaccion.component.html',
  styleUrls: ['./transaccion.component.css']
})
export class TransaccionComponent  {

  transaccion: Transaccion[] = [];
  
  constructor(private transaccionService: TransaccionService) {}

  ngOnInit(): void {
    this.obtenerTransaccion();
  }

  private obtenerTransaccion() {
    this.transaccionService.obtenerListaDeTransaccions().subscribe(dato => {
      this.transaccion = dato;
    });
  }

}

