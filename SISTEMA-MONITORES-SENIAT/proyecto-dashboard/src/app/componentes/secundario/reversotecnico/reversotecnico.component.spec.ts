import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReversotecnicoComponent } from './reversotecnico.component';

describe('ReversotecnicoComponent', () => {
  let component: ReversotecnicoComponent;
  let fixture: ComponentFixture<ReversotecnicoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReversotecnicoComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ReversotecnicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
