import { TestBed } from '@angular/core/testing';

import { ReversotecnicoService } from './reversotecnico.service';

describe('ReversotecnicoService', () => {
  let service: ReversotecnicoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReversotecnicoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
