import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, catchError, of } from 'rxjs';
import { Transaccion } from '../../transaccion';


// NUEVO MIRA CON INTERFACE 
@Injectable({
  providedIn: 'root'
})
export class TransaccionService {

  private baseURL = "http://localhost:8080/api/v1/transaccions/";

    constructor(private httpClient: HttpClient) { }
    // Este metodo sirve para obtener las transaccion
    obtenerListaDeTransaccions(): Observable<Transaccion[]> {
      return this.httpClient.get<Transaccion[]>(`${this.baseURL}`);
    }
}
