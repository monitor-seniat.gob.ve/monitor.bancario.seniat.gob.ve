import { Injectable } from '@angular/core';
import { Reportecierre } from '../../reportecierre';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ReportecierreService {

  private baseURL = "http://localhost:8080/api/v1/reportecierre/";

    constructor(private httpClient: HttpClient) { }
    // Este metodo sirve para obtener las transaccion
    obtenerListaDeReportecierres(): Observable<Reportecierre[]> {
      return this.httpClient.get<Reportecierre[]>(`${this.baseURL}`);
    }
}

