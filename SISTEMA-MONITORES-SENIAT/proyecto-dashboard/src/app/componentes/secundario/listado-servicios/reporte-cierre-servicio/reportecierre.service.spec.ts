import { TestBed } from '@angular/core/testing';

import { ReportecierreService } from './reportecierre.service';

describe('ReportecierreService', () => {
  let service: ReportecierreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReportecierreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
