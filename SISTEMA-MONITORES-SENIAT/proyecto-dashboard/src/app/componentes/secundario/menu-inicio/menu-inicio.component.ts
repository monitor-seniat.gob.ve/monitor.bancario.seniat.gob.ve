import { Component } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-menu-inicio',
  templateUrl: './menu-inicio.component.html',
  styleUrl: './menu-inicio.component.css'
})
export class MenuInicioComponent {
  isSidebarExpanded = false;
  expanded: boolean = true;

  toggleSidebar() {
    this.isSidebarExpanded = !this.isSidebarExpanded;
  }

  

  toggleDropdown() { // Declaración del método dentro de la clase
    this.expanded = this.expanded;
  }
}


