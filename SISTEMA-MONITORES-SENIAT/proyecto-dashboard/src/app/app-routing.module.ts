import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './componentes/principal/login/login.component';
import { MenuInicioComponent } from './componentes/secundario/menu-inicio/menu-inicio.component';
import { TransaccionComponent } from './componentes/secundario/transaccion/transaccion.component';
import { ListarTransaccionComponent } from './componentes/secundario/listar-transaccion/listar-transaccion.component';
import { ReportecierreComponent } from './componentes/secundario/reportecierre/reportecierre.component';
import { ReversotecnicoComponent } from './componentes/secundario/reversotecnico/reversotecnico.component';



const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'menu-inicio', component: MenuInicioComponent},
  { path: 'transaccion', component: TransaccionComponent },
  { path: 'listar-transaccion', component: ListarTransaccionComponent},
  { path: 'reportecierre', component: ReportecierreComponent},
  { path: 'reversotecnico', component: ReversotecnicoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
