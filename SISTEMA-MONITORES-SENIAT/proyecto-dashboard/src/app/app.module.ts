import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { MatToolbarModule } from '@angular/material/toolbar';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './componentes/principal/login/login.component';
import { MenuInicioComponent } from './componentes/secundario/menu-inicio/menu-inicio.component';
import { TransaccionComponent } from './componentes/secundario/transaccion/transaccion.component';
import { ReversotecnicoComponent } from './componentes/secundario/reversotecnico/reversotecnico.component';
import { ReportecierreComponent } from './componentes/secundario/reportecierre/reportecierre.component';
import { MantenimientoComponent } from './componentes/principal/mantenimiento/mantenimiento.component';
import { AlertasporvistaComponent } from './componentes/principal/alertasporvista/alertasporvista.component';
import { ListarTransaccionComponent } from './componentes/secundario/listar-transaccion/listar-transaccion.component';
import { SeguridadusuarioComponent } from './componentes/principal/registro/seguridadusuario/seguridadusuario.component';






@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MenuInicioComponent,
    TransaccionComponent,
    ReversotecnicoComponent,
    ReportecierreComponent,
    MantenimientoComponent,
    AlertasporvistaComponent,
    ListarTransaccionComponent,
    SeguridadusuarioComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatToolbarModule,
    HttpClientModule


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
