import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './componentes/secundario/dashboard.component';
import { LoginComponent } from './componentes/principal/login.component';
import { SeguridadUsuarioComponent } from './componentes/principal/registro/seguridad-usuario/seguridad-usuario.component';
import { CierresGeneralesComponent } from './componentes/secundario/cierres-generales/cierres-generales.component';
import { TransaccionComponent } from './componentes/secundario/transaccion-procesadas/transaccion/transaccion.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },      
  { path: 'dashboard', component: DashboardComponent },
  { path: 'registro/seguridad-usuario', component: SeguridadUsuarioComponent },
  { path: 'cierres-generales', component: CierresGeneralesComponent },  
  { path: 'transaccion', component: TransaccionComponent }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
 //acomodar rutas, crear otro componet y tomar guia de la clase ya que redirecciono bien