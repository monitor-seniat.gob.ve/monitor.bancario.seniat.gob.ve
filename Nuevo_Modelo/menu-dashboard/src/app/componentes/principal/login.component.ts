import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
password: any;
email: any;

constructor(private fb: FormBuilder, private router: Router) {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }
  onSubmit() {
    if (this.loginForm.valid) {
      // Perform login logic with the form values
      const email = this.loginForm.get('email')?.value;
      const password = this.loginForm.get('password')?.value;

      // Replace with your actual authentication call (e.g., service call)
      if (email === 'user@example.com' && password === '123456') {
        // Authentication successful
        this.router.navigate(['/dashboard']);
      } else {
        // Authentication failed
        // Show an error message to the user
      }
    } else {
      // Handle form validation errors
    }
  }
}