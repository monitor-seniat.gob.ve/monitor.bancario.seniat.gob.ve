import { Component, OnInit } from '@angular/core';
import { Transaccion } from './modelo/transaccion';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  showTable: boolean = false;
  busqueda: string;
  transaccion: Transaccion[] = []; 

  ngOnInit(): void {
  }
  onSubmit() {
    // Filtrar la lista de transacciones por la propiedad 'busqueda'
    this.transaccion = this.transaccion.filter(transaccion => {
      return transaccion.id_transaccion.toString().includes(this.busqueda) ||
             transaccion.banco.toString().includes(this.busqueda) ||
             transaccion.rif.toLocaleString().includes(this.busqueda) ||
             transaccion.periodo.toString().includes(this.busqueda) ||
       transaccion.monto.toString().includes(this.busqueda) ||
       transaccion.codigo_procesada.toString().includes(this.busqueda) ||
       transaccion.nro_documento.toString().includes(this.busqueda) ||
       transaccion.forma_documento.toString().includes(this.busqueda)
      //  transaccion.id_usuario.toString().includes(this.busqueda) ||
      //  transaccion.agencia.toString().includes(this.busqueda) ||
      //  transaccion.tipo_transaccion.toString().includes(this.busqueda) ||
      //  transaccion.fecha_transaccion.toString().includes(this.busqueda); ||
    });
  }
  // **Función para mostrar detalles del movimiento:**
  obtenerListaDeTransaccions(transaccions: Transaccion) {
    console.log("Ver detalle de cada transaccion:", transaccions);
    // Implementar la lógica para mostrar el detalle de cada transaccion
    // ...
  }


}
