import { Component } from '@angular/core';
import { Transaccion } from '../modelo/transaccion';


@Component({
  selector: 'app-transaccion',
  templateUrl: './transaccion.component.html',
  styleUrl: './transaccion.component.css'
})
export class TransaccionComponent {

  transaccions: Transaccion[] = [];
  
  ngOnInit(): void {
    this.obtenerTransaccionById();
  }
  //ojo/
  obtenerTransaccionById() {
    throw new Error('Method not implemented.');
  }
 //ojo//

  // **Función para mostrar detalles del movimiento:**
  obtenerlistarTodasLasTransaccion(transaccion: Transaccion) {
    console.log("Ver detalle de cada transaccion:", transaccion);
    // Implementar la lógica para mostrar el detalle de cada transaccion
    // ...
  }

  // **Función para editar el movimiento:**
  actualizarTransaccion(transaccion: Transaccion) {
    console.log("Editar transaccion:", transaccion);
    // Implementar la lógica para editar el movimiento
    // ...
  }

  // **Función para eliminar el movimiento:**
  eliminarTransaccion(transaccion: Transaccion) {
    console.log("Eliminar transaccion:", transaccion);
    // Implementar la lógica para eliminar el movimiento
    // ...
  }

}