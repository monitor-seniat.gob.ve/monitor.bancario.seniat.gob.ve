import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CierresGeneralesComponent } from './cierres-generales.component';

describe('CierresGeneralesComponent', () => {
  let component: CierresGeneralesComponent;
  let fixture: ComponentFixture<CierresGeneralesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CierresGeneralesComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CierresGeneralesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
