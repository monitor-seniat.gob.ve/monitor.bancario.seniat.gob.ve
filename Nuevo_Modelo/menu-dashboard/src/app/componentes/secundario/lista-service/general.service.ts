


// NUEVO MIRA CON INTERFACE 

import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, catchError, of } from 'rxjs';
import { Transaccion } from '../modelo/transaccion';


@Injectable({
  providedIn: 'root'
})
export class GeneralService {

  private baseURL = "http://localhost:8080/api/v1/transaccion/"; // Updated URL

  constructor(private httpClient: HttpClient) { }

  obtenerlistarTodasLasTransaccion(): Observable<Transaccion[]> {
    return this.httpClient.get<Transaccion[]>(`${this.baseURL}`);
  }

  crearTransaccion(transaccion: Transaccion): Observable<Object> {
    return this.httpClient.post(`${this.baseURL}`, transaccion);
  }

  actualizarTransaccion(id_transaccion: number, transaccion: Transaccion): Observable<Object> {
    return this.httpClient.put(`${this.baseURL}/${id_transaccion}`, transaccion);
  }

  obtenerTransaccionById(id_transaccion: number): Observable<Transaccion> {
    return this.httpClient.get<Transaccion>(`${this.baseURL}/${id_transaccion}`);
  }

  eliminarTransaccion(id_transaccion: number): Observable<Object> {
    return this.httpClient.delete(`${this.baseURL}/${id_transaccion}`).pipe(
      catchError((error: HttpErrorResponse) => {
        console.error(error);
        return of<Object>({ error: error.message });
      })
    );
  }
}
