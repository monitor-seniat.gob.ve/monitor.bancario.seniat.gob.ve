export class Transaccion {
     //citando a la la clase Transaccion
 //tabla      //clase
 transaccions: Transaccion[] = []; 

 id_transaccion: number;
 banco: string; // Change 'number' to 'string' for bank name
 rif: string; // Change 'number' to 'string' for tax ID
 periodo: string; // Assuming you want 'periodo' as a string representation of the date
 monto: number;
 codigo_procesada: string;
 nro_documento: string;
 forma_documento: string;
 id_usuario: number;
 agencia: string;
 tipo_transaccion: string;
 fecha_transaccion: Date;

}
