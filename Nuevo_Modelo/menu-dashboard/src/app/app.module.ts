import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatTableModule } from '@angular/material/table';
import { MatNativeDateModule } from '@angular/material/core'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { DashboardComponent } from './componentes/secundario/dashboard.component';
import { LoginComponent } from './componentes/principal/login.component';
import { FormsModule } from '@angular/forms';
import { SeguridadUsuarioComponent } from './componentes/principal/registro/seguridad-usuario/seguridad-usuario.component';
import { CierresGeneralesComponent } from './componentes/secundario/cierres-generales/cierres-generales.component';
import { TransaccionComponent } from './componentes/secundario/transaccion-procesadas/transaccion/transaccion.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    SeguridadUsuarioComponent,    
    CierresGeneralesComponent,    
    TransaccionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,    
    MatTableModule,
    FormsModule,
    MatNativeDateModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
