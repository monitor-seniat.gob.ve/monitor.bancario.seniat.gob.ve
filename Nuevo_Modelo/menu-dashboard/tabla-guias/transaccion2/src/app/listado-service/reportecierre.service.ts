import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Reportecierre } from '../modelo/reportecierre';

@Injectable({
  providedIn: 'root'
})
export class ReportecierreService {

  private baseURL = "http://localhost:8080/api/v1/reportecierre/";

    constructor(private httpClient: HttpClient) { }
    // Este metodo sirve para obtener las transaccion
    obtenerListaDeReportecierres(): Observable<Reportecierre[]> {
      return this.httpClient.get<Reportecierre[]>(`${this.baseURL}`);
    }
}

