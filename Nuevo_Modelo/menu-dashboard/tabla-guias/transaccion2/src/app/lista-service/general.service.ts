


// NUEVO MIRA CON INTERFACE 

import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, catchError, of } from 'rxjs';
import { Transaccion } from '../transaccion';


@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  obtenerListaDeReportecierre: any;
  obtenerListaDeReportecierres() {
    throw new Error('Method not implemented.');
  }

  private baseURL = "http://localhost:8080/api/v1/transaccions/";

    constructor(private httpClient: HttpClient) { }
    // Este metodo sirve para obtener las transaccion
    obtenerListaDeTransaccions(): Observable<Transaccion[]> {
      return this.httpClient.get<Transaccion[]>(`${this.baseURL}`);
    }
}
