import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashTemplateComponent } from './dash-template/dash-template.component';
import { TransaccionComponent } from './transaccion/transaccion.component';
import { ListarTransaccionComponent } from './listar-transaccion/listar-transaccion.component';
import { HttpClientModule } from '@angular/common/http';
import { ReportecierreComponent } from './reportecierre/reportecierre.component';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { MatToolbarModule } from '@angular/material/toolbar';




@NgModule({
  declarations: [
    AppComponent,    
    DashTemplateComponent,        
    TransaccionComponent,
    ListarTransaccionComponent,
    ReportecierreComponent
    
    
    
  ],
  imports: [
    BrowserModule,
    MatToolbarModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    provideAnimationsAsync()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
