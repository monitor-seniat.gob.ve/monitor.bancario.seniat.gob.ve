import { Component } from '@angular/core';
import { Reportecierre } from '../modelo/reportecierre';
import { ReportecierreService } from '../listado-service/reportecierre.service';

@Component({
  selector: 'app-reportecierre',
  templateUrl: './reportecierre.component.html',
  styleUrl: './reportecierre.component.css'
})
export class ReportecierreComponent {

  reportecierre: Reportecierre[] = [];
  
  constructor(private reportecierreService: ReportecierreService) {}

  ngOnInit(): void {
    this.obtenerReportecierre();
  }

  private obtenerReportecierre() {
    this.reportecierreService.obtenerListaDeReportecierres().subscribe(dato => {
      this.reportecierre = dato;
    });
  }

}


