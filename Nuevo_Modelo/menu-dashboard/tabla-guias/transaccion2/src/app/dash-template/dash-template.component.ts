import { Component, OnInit } from '@angular/core';
import { Transaccion } from '../transaccion';
import { GeneralService } from '../lista-service/general.service';

@Component({
  selector: 'app-dash-template',
  templateUrl: './dash-template.component.html',
  styleUrl: './dash-template.component.css'
})
export class DashTemplateComponent implements OnInit{

  title = 'Dashboard';
  
  transaccion: Transaccion[] = [];
  
  constructor(private generalService: GeneralService) {}

  ngOnInit(): void {
    this.obtenerTransaccion();
  }

  private obtenerTransaccion() {
    this.generalService.obtenerListaDeTransaccions().subscribe(dato => {
      this.transaccion = dato;
    });
  }

}