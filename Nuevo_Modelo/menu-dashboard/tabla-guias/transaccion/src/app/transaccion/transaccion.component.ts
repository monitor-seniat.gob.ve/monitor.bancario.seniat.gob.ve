import { Component, OnInit } from '@angular/core';
import { Transaccion } from '../transaccion';
import { GeneralService } from '../lista-service/general.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-transaccion',
  templateUrl: './transaccion.component.html',
  styleUrls: ['./transaccion.component.css']
})
export class TransaccionComponent  {

  transaccion: Transaccion[] = [];
  
  constructor(private generalService: GeneralService) {}

  ngOnInit(): void {
    this.obtenerTransaccion();
  }

  private obtenerTransaccion() {
    this.generalService.obtenerListaDeTransaccions().subscribe(dato => {
      this.transaccion = dato;
    });
  }

}

