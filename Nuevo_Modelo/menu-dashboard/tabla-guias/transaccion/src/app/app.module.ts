import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashTemplateComponent } from './dash-template/dash-template.component';
import { TransaccionComponent } from './transaccion/transaccion.component';
import { ListarTransaccionComponent } from './listar-transaccion/listar-transaccion.component';
import { HttpClientModule } from '@angular/common/http';
import { ReportecierreComponent } from './reportecierre/reportecierre.component';


@NgModule({
  declarations: [
    AppComponent,
    DashTemplateComponent,        
    TransaccionComponent,
    ListarTransaccionComponent,
    ReportecierreComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
