import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashTemplateComponent } from './dash-template/dash-template.component';
import { TransaccionComponent } from './transaccion/transaccion.component';
import { ListarTransaccionComponent } from './listar-transaccion/listar-transaccion.component';
import { ReportecierreComponent } from './reportecierre/reportecierre.component';


const routes: Routes = [
  
  
  { path: 'dash-template', component: DashTemplateComponent },
  { path: 'transaccion', component: TransaccionComponent },
  { path: 'listar-transaccion', component: ListarTransaccionComponent},
  { path: 'reportecierre', component: ReportecierreComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
