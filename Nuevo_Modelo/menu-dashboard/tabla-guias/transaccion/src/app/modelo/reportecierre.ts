export class Reportecierre {

    reportecierre: Reportecierre[] = [];
    id_cierres: number;
    numero_de_reportes: string;
    total_por_reporte: number;
    fecha_reporte: Date;
    nombre_reporte: string;
    total_aprobadas: string;
    total_reversadas: string;
    id_reverso: number;
    id_distribuccion: number;
}
