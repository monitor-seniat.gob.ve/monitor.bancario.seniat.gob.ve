import { Component, OnInit } from '@angular/core';
import { Transaccion } from '../transaccion';
import { GeneralService } from '../lista-service/general.service';

@Component({
  selector: 'app-listar-transaccion',
  templateUrl: './listar-transaccion.component.html',
  styleUrl: './listar-transaccion.component.css'
})
export class ListarTransaccionComponent implements OnInit{

  transaccion: Transaccion[] = [];
  
  constructor(private generalService: GeneralService) {}

  ngOnInit(): void {
    this.obtenerTransaccion();
  }

  private obtenerTransaccion() {
    this.generalService.obtenerListaDeTransaccions().subscribe(dato => {
      this.transaccion = dato;
    });
  }

}
