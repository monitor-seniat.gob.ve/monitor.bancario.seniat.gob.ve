import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportecierreComponent } from './reportecierre.component';

describe('ReportecierreComponent', () => {
  let component: ReportecierreComponent;
  let fixture: ComponentFixture<ReportecierreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReportecierreComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ReportecierreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
